# Project Ideas

> A list of ideas for projects that I felt the urge to jot down.

## Modern IRC client (web and mobile apps)

## GitPub - something like GitHub or GitLab, but super simple

GitPub will be a hosted website, where repositories can be browser like on GitHub and GitLab.
All repository-related metadata will be stored either directly in the repository (conflicts?) or in a separate repository.

I will eventually add GitPub support to Bloggo for local instances.
I will also run a local instance of Bloggo which in addition to GitHub and GitLab will see my local GitPub.

[GitLab repository](https://gitlab.com/TomasHubelbauer/gitpub)

## Git Annotation

A tool for annotating source code lines and files in Git repositories without creating any Git objects.
The aim is personal learning as opposed to contributing to the repository (so it works on readonly repositories as well).
Maybe there is a way to store Git objects only locally so they don't get `push`ed, that'd work, too.
The comments are assigned to lines by commit ID, file path and line number which is a stable address.
Opportunity for interlinking and rich media attachments.

## VS Code Extension for MarkDown todos

Scans all the MarkDown files in the workspace and lists them in a pane under Explorer.

[GitHub repository](https://github.com/TomasHubelbauer/vscode-markdown-todo)

## VS Code Extension for box drawing

[GitHub repository](https://github.com/TomasHubelbauer/vscode-box-drawing)

## EditorJS Restart

JavaScript text editor not utilizing `contenteditable`.
